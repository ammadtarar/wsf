/**
 * Import custom-env and set the env
 */
require("custom-env").env(process.env.NODE_ENV);
/**
 * import custom error class to make it accessible globally
 */
require("./utils/custom-error");

/**
 * App PORT
 */
const PORT = process.env.PORT || 3001;


/**
 * FORCE variable for env variarble. resets database if its true
 */
const force = process.env.FORCE || false;


/**
 * import server instance
 */
const app = require("./server");

/**
 * import db instance to init the at runtime
 */
const db = require("./controllers/db.js");

/**
 * Initialize db and run the app 
 */
db.sequelize
    .sync({
        force: process.env.NODE_ENV === "production" ? false : force,
    })
    .then(() => {
        app.listen(PORT, () => {
            if (process.env.ENV === 'test') return
            console.log("=======================================");
            console.log(`=== APP IS LISTENING ON PORT : ${PORT} ===`);
            console.log("=======================================");
        });
    });




module.exports = app;