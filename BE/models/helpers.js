get = (model, params) => {
    return new Promise((resolve, reject) => {
        model.findOne(params)
            .then(res => {
                resolve({
                    data: res,
                    error: null
                });
            })
            .catch(err => {
                resolve({
                    data: null,
                    error: err
                })
            })
    });
}

getAll = (model, params) => {
    return new Promise((resolve, reject) => {
        model.findAll(params)
            .then(res => {
                resolve({
                    data: res,
                    error: null
                });
            })
            .catch(err => {
                resolve({
                    data: null,
                    error: err
                })
            })
    });
}


getAllAndCount = (model, params) => {
    return new Promise((resolve, reject) => {
        model.findAndCountAll(params)
            .then(res => {
                resolve({
                    data: res,
                    error: null
                });
            })
            .catch(err => {
                console.log('ERR');
                console.log(err);
                resolve({
                    data: null,
                    error: err
                })
            })
    });
}


add = (model, params) => {
    return new Promise((resolve, reject) => {
        if (!params || Object.keys(params).length <= 0) {
            resolve({
                data: null,
                error: new CustomError('Request body cannot be empty', 422)
            })
            return
        }
        model.create(params)
            .then(res => {
                resolve({
                    data: res,
                    error: null
                });
            })
            .catch(err => {
                resolve({
                    data: null,
                    error: err
                })
            })
    });
}

bulkAdd = (model, params) => {
    return new Promise((resolve, reject) => {
        model.bulkCreate(params)
            .then(res => {
                resolve({
                    data: res,
                    error: null
                });
            })
            .catch(err => {
                resolve({
                    data: null,
                    error: err
                })
            })
    });
}


modify = (model, params) => {
    return new Promise((resolve, reject) => {
        model.update(params)
            .then(res => {
                resolve({
                    data: res,
                    error: null
                });
            })
            .catch(err => {
                resolve({
                    data: null,
                    error: err
                })
            })
    });
}

remove = (model, params) => {
    return new Promise((resolve, reject) => {
        model.destroy(params)
            .then(res => {
                resolve({
                    data: res,
                    error: null
                });
            })
            .catch(err => {
                resolve({
                    data: null,
                    error: err
                })
            })
    });
}






module.exports = {
    get,
    getAll,
    getAllAndCount,
    add,
    bulkAdd,
    modify,
    remove
}