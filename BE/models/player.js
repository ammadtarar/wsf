
const BaseModel = require('./BaseModel');
const { PLAYER_POSITIONS } = require("../utils/constants")

module.exports = function (sequelize, DataTypes) {
    class Player extends BaseModel { }

    Player.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        position: {
            type: DataTypes.ENUM(PLAYER_POSITIONS),
            values: PLAYER_POSITIONS,
            allowNull: false,
            validate: {
                isIn: {
                    args: [PLAYER_POSITIONS],
                    msg: `Incorrect player position. Allowed player positions are : ${PLAYER_POSITIONS.toString()}`
                }
            }
        },
        odds: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
        margin: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
    }, {
        sequelize,
        modelName: 'player',
        underscored: true
    });

    return Player;
};