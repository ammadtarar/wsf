const { Router } = require('express');
const app = Router();
const db = require('../../controllers/db.js');
const underscore = require('underscore');

app.post('/create', async (req, res, next) => {
    /* 	#swagger.tags = ['Player']
        #swagger.description = 'Endpoint create a new player'
        #swagger.parameters['object'] = {
            in: 'body',
            description: 'Player information.',
            required: true,
            schema: { $ref: "#/definitions/Player" }
        } 
        #swagger.responses[200] = {
            schema: { message : 'Player added successfully'},
            description: 'Player added successfully' 
        } 
    */
    let body = underscore.pick(req.body, 'name', 'position', 'margin');
    body.odds = 1;
    let { data, error } = await db.player.add(body);
    if (error) {
        next(error);
        return
    }
    res.json({
        message: 'Player added successfully'
    });
});

app.get('/list', async (req, res, next) => {
    /* 	#swagger.tags = ['Player']
        #swagger.description = 'Returns array of players' 
        #swagger.responses[200] = {
            schema: { 
                count : 1 , 
                data : [{ "$ref": "#/definitions/Player" }]
            }} 
    */
    let { data, error } = await db.player.getAllAndCount();
    if (error) {
        next(error);
        return
    }
    res.json(data);
});


app.patch('/:id/set/margin/:margin', async (req, res, next) => {
    /* 	#swagger.tags = ['Player']
        #swagger.description = 'Sets odds for the player based on margin'
        #swagger.responses[200] = {
            schema: { message : 'Player odds updated succesfully'},
            description: 'Player updated successfully' 
        } 
    */
    let id = parseInt(req.params.id);
    if (id === undefined || id === null || id <= 0) {
        throw new CustomError('Please send player id in url. For example /player/:id/set/margin/:margin', 400);
    }
    let margin = parseFloat(req.params.margin);



    let { data, error } = await db.player.get({ where: { id: id } });

    if (error) {
        next(error)
        return
    }

    let newOdds = data.odds / (1 + margin);
    console.log("🚀 ~ file: index.js ~ line 63 ~ app.patch ~ margin", margin)
    console.log("🚀 ~ file: index.js ~ line 76 ~ app.patch ~ data.odds", data.odds)
    console.log("🚀 ~ file: index.js ~ line 75 ~ app.patch ~ newOdds", newOdds)
    data = Object.assign(data, {
        odds: newOdds,

        margin: margin
    })
    data.save()

    res.json({
        message: 'Player odds updated succesfully'
    });


});




module.exports = app;