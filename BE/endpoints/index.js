const express = require('express');
const server = express();

//ROUTES
server.use('/player', require('./player/index.js'));

module.exports = server;