
const swaggerAutogen = require('swagger-autogen')();


const doc = {
    info: {
        version: "1.0.0",
        title: 'WallStreet Football',
        description: 'API documentation',
    },
    host: "localhost:3000",
    basePath: "/",
    schemes: ['http', 'https'],
    consumes: ['application/json'],
    produces: ['application/json'],
    tags: [
        {
            "name": "Player",
            "description": "Endpoints"
        }
    ],
    definitions: {
        Player: {
            $name: 'Ronaldo',
            $position: "Goalkeeper",
            odds: 0.0,
            margin: 0.0
        }
    }
}




const outputFile = "./swagger-docs/docs.json";
const endpointsFiles = ["./endpoints/index.js"];

swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
    require('./index.js');
    console.log("===============================================================");
    console.log(`=== API Docs available at :  http://localhost:${process.env.PORT}/api-docs ===`);
    console.log("===============================================================");
})
