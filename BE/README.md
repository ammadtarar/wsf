# WallStreet Football 
## _Backend server app_
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This directory contains the backend app for WallStreet Football interview test project.,

## Frameworks

- Node.js
- Express.js
- Sequelize & sqlite
- Jest (for testing)
- Swagger (for api documentation)


## Installation

The app has following commands :

| Command | Description |
| ------ | ------ |
| npm run local | This will run the app on port 8080 using local env |
| npm run test | This will run the jest command to run tests for endpoints |
| npm run swagger-autogen | This will run swagger script. It generates realtime api docs and prints a url that you can open in browser |

## Prequilities

Install all dependencies for the project :
```sh
npm install
```


## Starting server

```sh
npm install
npm run local
```

## License

MIT

**Ammad Amjad**

