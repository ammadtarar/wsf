

module.exports = function (db) {
    return {
        errorHandler: function (e, req, res, next) {
            if (e instanceof db.Sequelize.ForeignKeyConstraintError) {
                res.status(409).json({
                    message: "Incorrect ID passed. Please make sure the ID is valid and exists"
                })
            } else if (e instanceof db.Sequelize.ValidationError) {
                const messages = {};
                e.errors.forEach((error) => {
                    let message;
                    switch (error.validatorKey) {

                        case 'isIn':
                            message = error.message
                            break
                        case 'isEmail':
                            message = error.value + ' is not a valid email address. Please enter a valid email';
                            break;
                        case 'isDate':
                            message = 'Please enter a valid date';
                            break;
                        case 'len':
                            if (error.validatorArgs[0] === error.validatorArgs[1]) {
                                message = 'Use ' + error.validatorArgs[0] + ' characters for ' + error.path;
                            } else {
                                message = 'Use between ' + error.validatorArgs[0] + ' and ' + error.validatorArgs[1] + ' characters for ' + error.path;
                            }
                            break;
                        case 'min':
                            message = 'Use a number greater or equal to ' + error.validatorArgs[0] + 'for ' + error.path;
                            break;
                        case 'max':
                            message = 'Use a number less or equal to ' + error.validatorArgs[0] + 'for ' + error.path;
                            break;
                        case 'isInt':
                            message = 'Please use an integer number for ' + error.path;
                            break;
                        case 'is_null':
                            message = error.path + ' is required. Please complete ' + error.path + ' field';
                            break;
                        case 'not_unique':
                            message = error.value != null ? error.value + ' is taken. Please choose another ' + error.path : error.message;
                            break;
                        case 'isUrl':
                            message = error.value != null ? error.value + ' is not a valid value for ' + error.path : error.message;
                            break;
                    }
                    messages["message"] = message;
                });
                res.status(422).json(messages)
            } else if (e instanceof EnumValidationError) {
                res.status(422).json({
                    message: res.__('not_a_valid_enum', { value: e.enteredValue, field: e.field, allowed_values: e.allowedValues })
                })
            } else if (e instanceof CustomError) {
                res.status(e.code || 500).json({
                    message: e.message
                })
            } else {
                res.status(e.status || 500).json({
                    message: e.message
                })
            }
        },
        logger: function (req, res, next) {
            next();
            if (process.env.ENV === 'test') return
            console.log("");
            console.log("");
            console.log("");
            console.log(`========================================================================`);
            console.log(`==================== I N C O M I N G   R E Q U E S T ===================`);
            console.log(`========================================================================`);
            console.log("");
            console.log(`* DATE :`);
            console.log("");
            console.log(`${new Date().toString()}`);
            console.log("");
            console.log("");
            console.log(`* URL :`);
            console.log("");
            console.log(`${req.method}`, "\x1b[37m", `- {req.protocol}://${req.headers.host}${req.baseUrl}${req.path}`);
            console.log("");
            console.log("");
            console.log(`* PARAMS :`);
            console.log("");
            console.log(JSON.parse(JSON.stringify(req.params)));
            console.log("");
            console.log("");
            console.log(`* QUERY :`);
            console.log("");
            console.log(JSON.parse(JSON.stringify(req.query)));
            console.log("");
            console.log("");
            console.log(`* HEADERS :`);
            console.log("");
            console.log(JSON.parse(JSON.stringify(req.headers)));
            console.log("");
            console.log("");
            console.log(`========================================================================`);
            console.log("");
            console.log("");
            console.log("");
        },
    };

};