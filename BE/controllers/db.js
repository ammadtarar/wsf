
const Sequelize = require("sequelize");

var sequelize = sequelize = new Sequelize(
    'root',
    'root',
    'root',
    {
        dialect: 'sqlite',
        storage: "/Users/tarar/Documents/Repo/WSF/BE/data/db.sqlite",
        logging: process.env.ENABLE_DB_LOGGING === 'true'
    }
);

var db = {};

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.Op = Sequelize.Op;

db.player = sequelize.import("../models/player.js");


module.exports = db;
