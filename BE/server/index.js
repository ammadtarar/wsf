/**
 * imprort required frameworks and files
 */
const express = require("express");
const server = express();
const db = require("../controllers/db.js");
const middleware = require("../controllers/middleware.js")(db);
let cors = require("cors");


/**
 * Add all middlewares 
 */
server.use(cors());
server.use(express.json());
server.use(express.urlencoded());
server.use(middleware.logger);
server.use('', require('../endpoints/index.js'));
server.use(middleware.errorHandler);


/**
 * SERVER API DOCS FROM SWAGGER
 */
const swaggerUi = require("swagger-ui-express")
const swaggerDoc = require("../swagger-docs/docs.json");
server.use(
    '/api-docs',
    swaggerUi.serve,
    swaggerUi.setup(swaggerDoc)
);

module.exports = server;
