const PLAYER_POSITIONS = [
    'GoalKeeper',
    'Defender',
    'Midfielder',
    'Forward'
];


module.exports = {
    PLAYER_POSITIONS: PLAYER_POSITIONS
}