'use strict';
/**
 * Custom enum validator error
 */
global.EnumValidationError = class EnumValidationError extends Error {
    /**
     * 
     * @param {string} message 
     * @param {string} field 
     * @param {array} allowedValues 
     * @param {string} enteredValue 
     */
    constructor(message, field, allowedValues, enteredValue) {
        super(message);
        this.name = "EnumValidationError";
        this.message = message;
        this.field = field;
        this.allowedValues = allowedValues;
        this.enteredValue = enteredValue;
    }
}

/**
 * Custom error with message and code for general purpose
 */
global.CustomError = class CustomError extends Error {
    /**
     * 
     * @param {string} message 
     * @param {int} code 
     */
    constructor(message, code) {
        super(message);
        this.name = "CustomError";
        this.message = message;
        this.code = code;
    }
}
