const app = require('../index')
const supertest = require('supertest')
const request = supertest(app)

describe('Player Endpoints', () => {



    it('Wait for app to start', async () => {
        await new Promise((resolve, reject) => {
            let timeout = setTimeout(() => {
                resolve(500)
            }, 500);
            timeout.unref()
        })
        expect(1).toBe(1);
    })

    it('Try to create player with empty body - Should Fail - 422', async () => {
        const res = await request
            .post('/player/create')
            .send()
        expect(res.statusCode).toEqual(422)

    });

    it('Try to create player with incorrect position - Should fail - 422', async () => {
        const res = await request
            .post('/player/create')
            .send({
                "name": "Ronaldo",
                "position": "Goal-Keeper",
            })
        expect(res.statusCode).toEqual(422)

    });

    it('Try to create player  - Should succeed - 200', async () => {
        const res = await request
            .post('/player/create')
            .send({
                "name": "Ronaldo",
                "position": "GoalKeeper",
            })
        expect(res.statusCode).toEqual(200)
    });


    it('Try fetching list of players - Should succeed and count should be 1 - 200', async () => {
        const res = await request
            .get('/player/list')
            .send()
        expect(res.statusCode).toEqual(200)
        expect(res.body.count).toEqual(1)
    });


})