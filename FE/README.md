# WallStreet Football 
## _Frontend vue app_
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This directory contains the a server that server the prod version of vue app. It also contains the client directory that contains all the dev code for the vue app..,

## Frameworks

- Node.js
- Express.js
- Vue3


## Installation

```sh
npm install
```

## Running dev app from source code

```sh
cd client
npm install
npm run serve
```


## Building prod app from source code

The following command generates dist folder that contaisn prod version of the app
```sh
cd client
npm install
npm run buuild
```

## Runnning prod app

```sh
npm install
npm start
```

## License

MIT

**Ammad Amjad**

