import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";

const app = createApp(App);
app.use(router);

import DKToast from 'vue-dk-toast';
app.use(DKToast);

import { HTTP, URLS } from "./controllers/Network.js";
app.config.globalProperties.$HTTP = HTTP;
app.config.globalProperties.$URLS = URLS;

app.mount("#app");
