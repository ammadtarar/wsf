import axios from "axios";
const baseURL = process.env.VUE_APP_SERVER_URL;

axios.defaults.baseURL = baseURL;

const HTTP = axios.create({
    baseURL: process.env.VUE_APP_API,
});

const URLS = {
    baseURL: baseURL,
    PLAYERS_LIST: "/player/list",
    CREATE_PLAYER: "/player/create",
    SET_MARGINS: "/player/:id/set/margin/:margin",
};

export { HTTP, URLS };
