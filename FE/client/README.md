# wsf-fe




## Dependencies

- [axios](https://ghub.io/axios): Promise based HTTP client for the browser and node.js
- [core-js](https://ghub.io/core-js): Standard library
- [vue](https://ghub.io/vue): The progressive JavaScript framework for building modern web UI.
- [vue-dk-toast](https://ghub.io/vue-dk-toast): Lightweight toast notification plugin for Vue 3
- [vue-router](https://ghub.io/vue-router): &gt; This is the repository for Vue Router 4 (for Vue 3)

## Dev Dependencies

- [@vue/cli-plugin-babel](https://ghub.io/@vue/cli-plugin-babel): babel plugin for vue-cli
- [@vue/cli-plugin-eslint](https://ghub.io/@vue/cli-plugin-eslint): eslint plugin for vue-cli
- [@vue/cli-plugin-router](https://ghub.io/@vue/cli-plugin-router): router plugin for vue-cli
- [@vue/cli-service](https://ghub.io/@vue/cli-service): local service for vue-cli projects
- [@vue/compiler-sfc](https://ghub.io/@vue/compiler-sfc): @vue/compiler-sfc
- [@vue/eslint-config-prettier](https://ghub.io/@vue/eslint-config-prettier): eslint-config-prettier for Vue CLI
- [babel-eslint](https://ghub.io/babel-eslint): Custom parser for ESLint
- [eslint](https://ghub.io/eslint): An AST-based pattern checker for JavaScript.
- [eslint-plugin-prettier](https://ghub.io/eslint-plugin-prettier): Runs prettier as an eslint rule
- [eslint-plugin-vue](https://ghub.io/eslint-plugin-vue): Official ESLint plugin for Vue.js
- [prettier](https://ghub.io/prettier): Prettier is an opinionated code formatter
- [sass](https://ghub.io/sass): A pure JavaScript implementation of Sass.
- [sass-loader](https://ghub.io/sass-loader): Sass loader for webpack

